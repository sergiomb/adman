# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.2.1 - 2020-01-06
- Fix issue where an expiring TGT can lead to a GSSAPI error (#10).
- Add -v option for 'user list' and 'group list'

## 0.2.0 - 2019-12-26
- Add pending password expiry notification feature
  - Added 'user checkexpire' command and related configuration
  - Added SMTP configuration
- Set default config and data paths
- Add --version option
- Fix exception when domain has no configured alternate UPN suffixes
- Refactor LdapObject code to simplify handling of known attributes

## 0.1.0 - 2019-07-18
- Add support for UPN suffix assignment via `user setupns` command
- Rename `assign`/`clear` to `assignids`/`clearids`
- Add `allmaint` command

## 0.0.2 - 2019-07-16
- Rename to "adman"

## 0.0.1 - 2019-06-23
- Initial release as "adam"
