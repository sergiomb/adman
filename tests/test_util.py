import unittest
from datetime import datetime, timezone
from adman.util import *

class Test_single(unittest.TestCase):
    def test_single_one(self):
        seq = [1]
        self.assertEqual(single(seq), 1)

    def test_single_one_match(self):
        seq = [1, 200]
        self.assertEqual(single(seq, lambda x: x > 10), 200)

    def test_single_empty(self):
        seq = []
        with self.assertRaises(IndexError):
            single(seq)

    def test_single_none_match(self):
        seq = [1, 2, 3]
        with self.assertRaises(IndexError):
            single(seq, lambda x: x > 10)

    def test_multiple(self):
        seq = [1, 2, 3]
        with self.assertRaises(IndexError):
            single(seq)

    def test_multiple_match(self):
        seq = [10, 20, 30]
        with self.assertRaises(IndexError):
            single(seq, lambda x: x > 10)

    def test_single_one_None(self):
        seq = [None]
        self.assertEqual(single(seq), None)


    def test_single_or(self):
        seq = []
        self.assertEqual(single_or(seq, 42), 42)



class Test_int_from_bytes(unittest.TestCase):
    def test_simple_big(self):
        data = b'\x01\x02\x03\x04'
        val = int_from_bytes(data, 0, 4, 'big')
        self.assertEqual(0x01020304, val)

    def test_simple_little(self):
        data = b'\x01\x02\x03\x04'
        val = int_from_bytes(data, 0, 4, 'little')
        self.assertEqual(0x04030201, val)

    def test_offset(self):
        data = b'\xFF\xFF\x01\x02\x03\x04'
        val = int_from_bytes(data, 2, 4, 'little')
        self.assertEqual(0x04030201, val)

    def test_extra_length(self):
        data = b'\xFF\xFF\x01\x02\x03\x04\xFF\xFF\xFF\xFF\xFF'
        val = int_from_bytes(data, 2, 4, 'little')
        self.assertEqual(0x04030201, val)

    def test_raise_on_short(self):
        data = b'\x01\x02\x03'
        with self.assertRaises(ValueError):
            int_from_bytes(data, 0, 4, 'little')


class Test_FILETIME(unittest.TestCase):
    # Friday, January 17, 2020 6:28:11pm
    SOME_FILETIME = 132237592910000000
    SOME_DATETIME = datetime(year=2020, month=1, day=17, hour=18, minute=28, second=11, tzinfo=timezone.utc)

    MAX_FILETIME = INT64_MAX

    def assertIsInUTC(self, dt):
        self.assertEqual(dt.tzinfo, timezone.utc)

    def test__FILETIME_to_datetime__negative(self):
        with self.assertRaises(ValueError):
            FILETIME_to_datetime(-1234)

    def test__FILETIME_to_datetime__normal(self):
        dt = FILETIME_to_datetime(self.SOME_FILETIME)
        self.assertEqual(dt, self.SOME_DATETIME)
        self.assertIsInUTC(dt)

    def test__FILETIME_to_datetime__overflow(self):
        dt = FILETIME_to_datetime(self.MAX_FILETIME)
        self.assertEqual(dt, UTCMAX)
        self.assertIsInUTC(dt)

    def test__datetime_to_FILETIME__normal(self):
        ft = datetime_to_FILETIME(self.SOME_DATETIME)
        self.assertEqual(ft, self.SOME_FILETIME)

    def test__datetime_to_FILETIME__max(self):
        ft = datetime_to_FILETIME(UTCMAX)
        self.assertEqual(ft, self.MAX_FILETIME)

    def test__datetime_to_FILETIME__requires_offset_aware(self):
        with self.assertRaisesRegex(TypeError, "can't \w* offset-naive and offset-aware datetimes") as ar:
            datetime_to_FILETIME(datetime.now())
